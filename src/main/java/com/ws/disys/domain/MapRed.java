package com.ws.disys.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

public class MapRed {
	public void testCommandJsch(String sintomas, String nombre) throws IOException, JSchException{
		 JSch jsch = new JSch();
		 Session session = jsch.getSession("hduser", "40.115.20.31", 22);
	     //UserInfo ui = new SUserInfo(pass, null);
	     //session.setUserInfo(ui);
	     session.setPassword("1Hadoop1");
	     Properties config = new Properties();
	     config.put("StrictHostKeyChecking", "no");
	     session.setConfig(config);
	     session.connect();
	     ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

	     InputStream in = channelExec.getInputStream();
	     Date date = new Date();
	     
	     System.out.println(date);
	     channelExec.setCommand("sh /home/hduser/runa.sh "+nombre+" '"+sintomas+"'");
	     channelExec.connect();

	     BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	     String linea = null;
	     int index = 0;

	     while ((linea = reader.readLine()) != null) {
	         System.out.println(++index + " : " + linea);
	     }

	     channelExec.disconnect();
	     session.disconnect();
	     Date date2 = new Date();
	     System.out.println(date2);
	     System.out.println((date2.getTime()-date.getTime())/1000);
	     System.out.println("------ FIN");
	 }
	
	public String read (String nombre) throws JSchException, IOException{
		JSch jsch = new JSch();
		 Session session = jsch.getSession("hduser", "40.115.20.31", 22);
	     //UserInfo ui = new SUserInfo(pass, null);
	     //session.setUserInfo(ui);
	     session.setPassword("1Hadoop1");
	     Properties config = new Properties();
	     config.put("StrictHostKeyChecking", "no");
	     session.setConfig(config);
	     session.connect();
	     ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

	     InputStream in = channelExec.getInputStream();
	     Date date = new Date();
	     
	     System.out.println(date);
	     channelExec.setCommand("sh /home/hduser/cat.sh out"+nombre+"/*");
	     channelExec.connect();

	     BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	     String linea = null;
	     int index = 0;
	     
	     String cadena="";
	     int cont=0;
	     while ((linea = reader.readLine()) != null && cont<10) {
	         cadena=cadena+";"+linea;
	    	 System.out.println(++index + " : " + linea);
	    	 cont++;
	     }
	     cadena = cadena.substring(1, cadena.length());
	     
	    channelExec.disconnect();
	     session.disconnect();
	     Date date2 = new Date();
	     System.out.println(date2);
	     System.out.println((date2.getTime()-date.getTime())/1000);
	     System.out.println("------ FIN");
		return cadena;
	}
	
	public String read2 (String nombre) throws JSchException, IOException{
		JSch jsch = new JSch();
		 Session session = jsch.getSession("hduser", "40.115.20.31", 22);
	     //UserInfo ui = new SUserInfo(pass, null);
	     //session.setUserInfo(ui);
	     session.setPassword("1Hadoop1");
	     Properties config = new Properties();
	     config.put("StrictHostKeyChecking", "no");
	     session.setConfig(config);
	     session.connect();
	     ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

	     InputStream in = channelExec.getInputStream();
	     Date date = new Date();
	     
	     System.out.println(date);
	     channelExec.setCommand("sh /home/hduser/cat.sh out"+nombre+"/*");
	     channelExec.connect();

	     BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	     String linea = null,cadena="";
	     
	     
	     TreeMap<Integer, String> tm= new TreeMap<Integer, String>();
	     
	     String cad[]=new String[2];
	     
	     String cad1="";
	     while ((linea = reader.readLine())!= null){
	    	 cad=linea.split(";");
	    	 System.out.println("Cad: "+cad[0]);   //"    12.0"
	    	 cad1=cad[1].trim();
	    	 tm.put(Integer.parseInt(cad1.substring(0, cad1.length()-2)), cad[0]);
	     }
	     
	     Iterator it=tm.values().iterator();
	     
	     System.out.println();
	     while (it.hasNext()){
	    	 Map.Entry me=(Entry) it.next();
	    	 System.out.println(me.getKey()+" - "+me.getValue());
	     }
	     
	     for (int q=0;q<10;q++){
	         Map.Entry me = tm.lastEntry();
	         cadena=cadena+";"+me.getValue() + ";"+me.getKey();
	         System.out.print(me.getValue() + ";");
	         System.out.println(me.getKey());
	         
	       }
	     
	     cadena = cadena.substring(1, cadena.length());
	     
	    channelExec.disconnect();
	     session.disconnect();
	     Date date2 = new Date();
	     System.out.println(date2);
	     System.out.println((date2.getTime()-date.getTime())/1000);
	     System.out.println("------ FIN");
		return cadena;
	}
	
}
