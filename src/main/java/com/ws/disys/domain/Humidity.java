package com.ws.disys.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Humidity {

    @Id
    private String id;

    private String humidity;

	private String hour;

    public Humidity(String id, String humidity, String hour) {
        this.id=id;
        this.humidity=humidity;
        this.hour=hour;
    }

    public Humidity() {
    
    }
    
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}
}