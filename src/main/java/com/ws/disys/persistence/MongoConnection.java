package com.ws.disys.persistence;

import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;
import com.ws.disys.domain.Humidity;
import com.mongodb.MongoClient;
import org.bson.Document;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.ServerAddress;

import java.util.ArrayList;
import java.util.Arrays;

public class MongoConnection {
	
	private final static String DB_USER="botanicgarden";
	private final static String DB_NAME="itsue";
	private final static String DB_PWD="garden";
	private final static String DB_SERVER="ds059654.mongolab.com";
	private final static int DB_PORT=59654;
	
	private static MongoConnection instance;
	private static MongoDatabase database;
	
	private MongoConnection() {
	    
	}

	public static synchronized MongoConnection getInstance() {
	    conexion();
		if (instance == null) {
	        instance = new MongoConnection();
	    }
	    return instance;
	}
	
	public static void conexion(){
		MongoCredential credential = MongoCredential.createCredential(DB_USER,DB_NAME,DB_PWD.toCharArray());
		@SuppressWarnings("resource")
		MongoClient client = new MongoClient(new ServerAddress(DB_SERVER,DB_PORT), Arrays.asList(credential));
		database = client.getDatabase(DB_NAME);
			
		System.out.println("Conexion created");
	}
	
	public ArrayList<Humidity> findAll (){
		//FindAll()
		ArrayList <Humidity> list=new ArrayList<Humidity>();
		FindIterable<Document> iterable = database.getCollection("HumidityMeasures").find();
		iterable.forEach(new Block<Document>() {
			@Override
			public void apply(final Document document) {
				list.add(new Humidity(document.getString("Id"),document.getString("Humidity"), document.getString("Date")));
			}
		});
		return list;
	}
	
	public void insert (String id, String humidity, String date){
		database.getCollection("HumidityMeasures").insertOne(
		    new Document("Id", id).append("Humidity", humidity+"%").append("Date", date));
			FindIterable<Document> iterable = database.getCollection("HumidityMeasures").find();
			iterable.forEach(new Block<Document>() {
				@Override
				public void apply(final Document document) {
					//System.out.println(document);
				}
			});
	}
}

/*

public class Mongo {
private static Mongo instance;
private Mongo() {
    ...
}

public static synchronized Mongo getInstance() {
    if (instance == null) {
        instance = new Mongo();
    }
    return instance;
}
}

Usage:    Mongo.getInstance();

*/