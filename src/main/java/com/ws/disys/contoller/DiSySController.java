package com.ws.disys.contoller;


import java.util.ArrayList;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.UserInfo;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Properties;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ws.disys.persistence.MongoConnection;
import com.ws.disys.domain.Humidity;
import com.ws.disys.domain.MapRed;


@RestController
//@RequestMapping(value="/")
public class DiSySController {
	//@Autowired
	//private IDictionaryService dictionaryService;
	
	/*@RequestMapping(value="/{id}")
	public Term getTerm(@PathVariable(value="id") String id){
	  @RequestMapping(value="/create", method = RequestMethod.POST)
	public void addTerm (@RequestBody Term t){*/
	
	@RequestMapping(value="/")
	public String principal(){
		return "hello disys";
	}
	
	@RequestMapping(value="/map")
	public String mapred() throws JSchException, IOException, URISyntaxException{
		MapRed mapred=new MapRed();
		String sintomas="tos;dolor de cabeza;fiebre";
		
		return "ok";
	}
	
	
	@RequestMapping(value="/enfermedades", method = RequestMethod.POST)
	public String diseases (@RequestBody String sint) throws IOException, JSchException, URISyntaxException{
		
		sint=sint.replace("%3B", ";");
		sint=sint.replace("=","");
		System.out.println(sint);
		MapRed mapred=new MapRed();
		//String sintomas="tos;dolor de cabeza;fiebre";
		
		Date ahora=new Date();
	    String nombre=ahora.getDay()+""+ahora.getHours()+""+ahora.getMinutes()+""+ahora.getSeconds();
	     
		mapred.testCommandJsch(sint,nombre);
		String resultado=mapred.read(nombre);
		
		return resultado;
		//return "contusion;89;fractura de rodilla;76;luxacion;68;esguince;50";
	}
	
	@RequestMapping(value="/{city}")
	public String city(@PathVariable(value="city") String city){
		return "gripe";
	}
	
	
	//********************************
	
	@RequestMapping(value="/read")
	public ArrayList<Humidity> read(){
		MongoConnection mongo=MongoConnection.getInstance();
		return mongo.findAll();
	}
}
